package edu.columbia.cs4160.pa2;


public class StickManFactory implements LinkTreeFactory {

	@Override
	public Link create() {

		double[] rootPos = {0,0,0};
		double[] rootDim = {0,0,0};
		
		double[] torsoPos = {0,0,0};
		double[] torsoDim = {1, .4, .2};
		
		double[] shoulderPos = {0,0,0};
		double[] shoulderDim = {.4, .2, .2};
		
		double[] armPos = {.4, 0, 0};
		double[] armDim = {.5, .2, .2};
		
		double[] thighPos = {1, 0, 0};
		double[] thighDim = {.6, .2, .2};
		
		double[] calfPos = {.6, 0, 0};
		double[] calfDim = {.6, .2, .2};
		
		Link root = new Link(rootPos, rootDim, null);
		root.setRotationZ(-90);
		root.preventRotations();
		
        Link torso = new Link(torsoPos, torsoDim, root);
        root.addChild(torso);
        
        Link arm11 = new Link(shoulderPos, shoulderDim, root);
        arm11.setRotationZ(30);
        root.addChild(arm11);
        
        Link arm12 = new Link(armPos, armDim, arm11);
        arm11.addChild(arm12);
        
        Link arm21 = new Link(shoulderPos, shoulderDim, root);
        arm21.setRotationZ(-30);
        root.addChild(arm21);
        
        Link arm22 = new Link(armPos, armDim, arm21);
        arm21.addChild(arm22);
        
        Link leg11 = new Link(thighPos, thighDim, torso);
        leg11.setRotationZ(30);
        torso.addChild(leg11);
        
        Link leg12 = new Link(calfPos, calfDim, leg11);
        leg11.addChild(leg12);
        
        Link leg21 = new Link(thighPos, thighDim, torso);
        leg21.setRotationZ(-30);
        torso.addChild(leg21);
        
        Link leg22 = new Link(calfPos, calfDim, leg21);
        leg21.addChild(leg22);
        
        
        return root;
		
	}

	@Override
	public String getModelName() {
		return "StickMan";
	}

}
