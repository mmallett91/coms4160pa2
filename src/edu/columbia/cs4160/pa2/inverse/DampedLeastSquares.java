package edu.columbia.cs4160.pa2.inverse;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Deque;
import java.util.LinkedList;

import org.ejml.simple.SimpleMatrix;

import edu.columbia.cs4160.pa2.Link;

public class DampedLeastSquares {
	
	public static final double LAMBDA = .5;

	/**
	 * 
	 * @param leaf the end effector we care about position
	 * 			we can walk up the parent tree to find the root
	 * @param target
	 * 			vector <x,y,z> indicating target position
	 */
	public static void iterate(Link leaf, Vector3d target){
		
//		Vector3d[] s = new Vector3d[2];
//		s[0] = new Vector3d(1,0,0);
//		s[1] = new Vector3d(2,0,0);
		
		LinkedList<Vector3d> effectors = new LinkedList<Vector3d>();
		leaf.getTreeEffectorCoordinates(effectors);
		
		Vector3d[] s = toArray(effectors);
		
//		Vector3d[] p = new Vector3d[2];
//		p[0] = new Vector3d(0,0,0);
//		p[1] = new Vector3d(1,0,0);
		
		LinkedList<Vector3d> joints = new LinkedList<Vector3d>();
		leaf.getTreeJointCoordinates(joints);
		
		Vector3d[] p = toArray(joints);
		
		SimpleMatrix jacobian = Jacobian.create(s, p);
		
		SimpleMatrix deltaP = new SimpleMatrix(3 * s.length,1);
		
		for(int i=0; i< s.length; i++){
//			deltaP.set(3 * i, 0, s[i].x);
//			deltaP.set(3 * i + 1, 0, s[i].y);
//			deltaP.set(3 * i + 2, 0, s[i].z);
			deltaP.set(3 * i, 0, 0);
			deltaP.set(3 * i + 1, 0, 0);
			deltaP.set(3 * i + 2, 0, 0);
		}
		
		
		Vector3d e = target.sub(s[s.length-1]);
		
		deltaP.set(deltaP.numRows() - 3, 0, e.x);
		deltaP.set(deltaP.numRows() - 2, 0, e.y);
		deltaP.set(deltaP.numRows() - 1, 0, e.z);
		
		//SimpleMatrix theta = jacobian.solve(p_delta);
		
		//System.out.println(theta);
		
		SimpleMatrix transpose = jacobian.transpose();
		
//		System.out.println("J " + jacobian);
		
		SimpleMatrix step1 = jacobian.mult(transpose);
		
//		System.out.println("step1 " + step1);
		
		SimpleMatrix step2 = SimpleMatrix.identity(step1.numCols()).scale(LAMBDA);
		
		SimpleMatrix step3 = step1.plus(step2);
		
		SimpleMatrix step4 = transpose.mult(step3.invert());
		
		SimpleMatrix theta = step4.mult(deltaP);
				
		Deque<Link> stack = new ArrayDeque<Link>();
		
		leaf.getTreeHierarchy(stack);
		
		int i = 0;
		while(!stack.isEmpty()){
			
			Link link = stack.pop();
			
			double rad = theta.get(i++);
			double deg = (rad * 180d) / Math.PI;
			
			link.updateRotationZ(deg);
			
		}
		
		
		//SimpleMatrix theta = a.solve(b);
		
		//System.out.println(theta);
		
		//SimpleMatrix step2 = step1.plus(dampI).invert();
		
		//System.out.println(step2);
		
		//SimpleMatrix step3 = step2.mult(transpose);
		
		//System.out.println(step3);
		
		/*
	
		go over the formula
		which form to use for solver?
		how to represent in jacobian? entries are scalar or vectors
		k vs m = 3k rows
		
		what is e vector? what values to use?
		

		 */
		
	}
	
	private static Vector3d[] toArray(Collection<Vector3d> collection){
		//because apparently the collection.toArray can't cast to Vector3d..
		Vector3d[] arr = new Vector3d[collection.size()];
		int i = 0;
		for(Vector3d v : collection){
			arr[i++] = v;
		}
		return arr;
	}
	
}
