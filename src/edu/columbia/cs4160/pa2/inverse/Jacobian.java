package edu.columbia.cs4160.pa2.inverse;

import org.ejml.simple.SimpleMatrix;

public class Jacobian {
	
	//unit vector is <0,0,1>
	//for simple case
	//	end effector s_i is <1,0,0>
	//	joint position p_j is <0,0,0>
	static Vector3d v_j;
	static{
		v_j = new Vector3d(0,0,1);
	}

	public static SimpleMatrix create(Vector3d[] s, Vector3d[] p){
		
		SimpleMatrix ret = new SimpleMatrix(s.length * 3, p.length);
		
		for(int i=0; i<s.length; i++){
			for(int j=0; j<p.length; j++){
				build(i, j, s[i], p[j], ret);
			}
		}
		
		return ret;
	}
	
	public static void build(int i, int j, Vector3d s_i, Vector3d p_j, SimpleMatrix m){
		
		Vector3d res = v_j.cross(s_i.sub(p_j));
		
		m.set(3*i, j, res.x);
		m.set(3*i+1, j, res.y);
		m.set(3*i+2, j, res.z);
		
	}
	
	
	public static void main(String args[]){
		
//		Vector3d[] s = new Vector3d[2];
//		s[0] = new Vector3d(1,0,0);
//		s[1] = new Vector3d(2,0,0);
//		
//		Vector3d[] p = new Vector3d[2];
//		p[0] = new Vector3d(0,0,0);
//		p[1] = new Vector3d(1,0,0);
//		
//		SimpleMatrix jacobian = create(s, p);
//		
//		SimpleMatrix deltaP = new SimpleMatrix(6,1);
//		
//		deltaP.setColumn(0, 1, 0, 0, 0, 1, 0);
//		
//		//SimpleMatrix theta = jacobian.solve(p_delta);
//		
//		//System.out.println(theta);
//		
//		SimpleMatrix transpose = jacobian.transpose();
//		
//		SimpleMatrix step1 = jacobian.mult(transpose);
//		
//		double lambda = .1;
//		
//		SimpleMatrix step2 = SimpleMatrix.identity(step1.numCols()).scale(lambda * lambda);
//		
//		SimpleMatrix step3 = step1.plus(step2);
//		
//		//System.out.println("B " + b);
//		
//		SimpleMatrix step4 = transpose.mult(step3.invert());
//		
//		SimpleMatrix theta = step4.mult(deltaP);
		
		//SimpleMatrix theta = a.solve(b);
		
		//System.out.println(theta);
		
		//SimpleMatrix step2 = step1.plus(dampI).invert();
		
		//System.out.println(step2);
		
		//SimpleMatrix step3 = step2.mult(transpose);
		
		//System.out.println(step3);
		
		/*
	
		go over the formula
		which form to use for solver?
		how to represent in jacobian? entries are scalar or vectors
		k vs m = 3k rows
		
		what is e vector? what values to use?
		

		 */
		
	}
	
	
	
	
	
}
