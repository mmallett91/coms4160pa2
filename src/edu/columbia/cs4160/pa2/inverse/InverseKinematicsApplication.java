package edu.columbia.cs4160.pa2.inverse;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.LinkedList;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;

import edu.columbia.cs4160.pa2.ControlsTexture;
import edu.columbia.cs4160.pa2.Link;
import edu.columbia.cs4160.pa2.LinkTreeFactory;
import edu.columbia.cs4160.pa2.SimpleLinearFactory;
import edu.columbia.cs4160.pa2.StickManFactory;

public class InverseKinematicsApplication {

    String windowTitle = "Inverse Kinematics";
    public boolean closeRequested = false;

    long lastFrameTime; // used to calculate delta
    
    float triangleAngle; // Angle of rotation for the triangles
    float quadAngle; // Angle of rotation for the quads

//    private Link link1, link2, link3, link32;
    
    private ArrayList<Link> links;
    private int linksIndex;
    private Link active, root;
    
    private boolean inPickMode;
    private boolean inSolveMode;
    
    static Vector3d target;
    static{
    	target = new Vector3d(
    		.5,
    		.5,
    		0
		);
    }
    
    public static final double TOLERANCE = .1;
    
    private void traverseTree(Link link){
    	
    	links.add(link);
    	
    	for(Link child : link.getChildren()){
    		traverseTree(child);
    	}
    	
    }
    
    private void nextActive(){
    	active.setActive(false);
    	if(++linksIndex >= links.size()){
    		linksIndex = 0;
    	}
    	active = links.get(linksIndex);
    	active.setActive(true);
    }
    
    private void prevActive(){
    	active.setActive(false);
    	if(--linksIndex < 0){
    		linksIndex = links.size() -1;
    	}
    	active = links.get(linksIndex);
    	active.setActive(true);
    }
    
    public void run() {

        createWindow();
        getDelta(); // Initialise delta timer
        initGL();
        
        inPickMode = true;
        inSolveMode = false;
        
        ControlsTexture.init();
        
        while (!closeRequested) {
            pollInput();
//            updateLogic(getDelta());
            if(inSolveMode){
            	DampedLeastSquares.iterate(active, target);
            	LinkedList<Vector3d> list = new LinkedList<Vector3d>();
            	active.getTreeEffectorCoordinates(list);
            	Vector3d activeCoords = list.getLast();
            	double error = activeCoords.sub(target).mag();
            	if(error < TOLERANCE){
            		inSolveMode = false;
            	}
            }
            		
            renderGL();

            Display.update();
        }
        
        cleanup();
    }
    
    private void initGL() {

        /* OpenGL */
        int width = Display.getDisplayMode().getWidth();
        int height = Display.getDisplayMode().getHeight();

        GL11.glViewport(0, 0, width, height); // Reset The Current Viewport
        GL11.glMatrixMode(GL11.GL_PROJECTION); // Select The Projection Matrix
        GL11.glLoadIdentity(); // Reset The Projection Matrix
        GLU.gluPerspective(45.0f, ((float) width / (float) height), 0.1f, 100.0f); // Calculate The Aspect Ratio Of The Window
        GL11.glMatrixMode(GL11.GL_MODELVIEW); // Select The Modelview Matrix
        GL11.glLoadIdentity(); // Reset The Modelview Matrix

        GL11.glShadeModel(GL11.GL_SMOOTH); // Enables Smooth Shading
        GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // Black Background
        GL11.glClearDepth(1.0f); // Depth Buffer Setup
        GL11.glEnable(GL11.GL_DEPTH_TEST); // Enables Depth Testing
        GL11.glDepthFunc(GL11.GL_LEQUAL); // The Type Of Depth Test To Do
        GL11.glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_NICEST); // Really Nice Perspective Calculations
        GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
//        Camera.create();        
        
    }
    
    private void renderGL() {

        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT); // Clear The Screen And The Depth Buffer
        GL11.glLoadIdentity(); // Reset The View
        GL11.glTranslatef(0.0f, 0.0f, -7.0f); // Move Right And Into The Screen
        
//        Camera.apply();
        if(inPickMode){
        	// draw pick ui
        	GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_FILL); 
        	ControlsTexture.draw(ControlsTexture.MODE.INVERSE);
        	GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
        }
        else{
        	target.draw();
        	root.draw();
        }

    }

    /**
     * Poll Input
     */
    public void pollInput() {
//        Camera.acceptInput(getDelta());
        // scroll through key events
        if(Keyboard.isKeyDown(Keyboard.KEY_Q)){
        	active.setRotationZ(active.getRotationZ() - 1d);
        }
        else if(Keyboard.isKeyDown(Keyboard.KEY_W)){
        	active.setRotationZ(active.getRotationZ() + 1d);
        }
        while (Keyboard.next()) {
            if (Keyboard.getEventKeyState()) {
                if (Keyboard.getEventKey() == Keyboard.KEY_ESCAPE)
                    closeRequested = true;
                else if (Keyboard.getEventKey() == Keyboard.KEY_P)
                    snapshot();
                else if(Keyboard.getEventKey() == Keyboard.KEY_TAB){
                	if(!inPickMode){
                		nextActive();
                	}
                }
                else if(Keyboard.getEventKey() == Keyboard.KEY_LSHIFT){
                	if(!inPickMode){
                		prevActive();
                	}
                }
                else if(inPickMode && Keyboard.getEventKey() == Keyboard.KEY_1){
                	// make a linear
                	inPickMode = false;
                	LinkTreeFactory ltf = new SimpleLinearFactory();
                    root = ltf.create();
                    
                    links = new ArrayList<Link>();
                    linksIndex = 0;
                    
                    traverseTree(root);
                    
                    active = links.get(linksIndex);
                    active.setActive(true);
                }
                else if(inPickMode && Keyboard.getEventKey() == Keyboard.KEY_2){
                	
                	inPickMode = false;
                	
                	LinkTreeFactory ltf = new StickManFactory();
                    root = ltf.create();
                    
                    links = new ArrayList<Link>();
                    linksIndex = 0;
                    
                    traverseTree(root);
                    
                    active = links.get(linksIndex);
                    active.setActive(true);
                }
                else if(!inPickMode && Keyboard.getEventKey() == Keyboard.KEY_LCONTROL){
                	inPickMode = true;
                }
                else if(Keyboard.getEventKey() == Keyboard.KEY_Z){
                	Link.displayMesh = !Link.displayMesh;
                }
//                else if(Keyboard.getEventKey() == Keyboard.KEY_X){
//                	inSolveMode = !inSolveMode;
//                }
//                else if(Keyboard.getEventKey() == Keyboard.KEY_C){
//                	GL11.glLoadIdentity(); // Reset The View
//                    LinkedList<Vector3d> list = new LinkedList<Vector3d>();
//                    active.getTreeJointCoordinates(list);
//                    System.out.println("Joints");
//                    for(Vector3d v : list){
//                    	System.out.println(v);
//                    }
//                    GL11.glLoadIdentity(); // Reset The View
//                    LinkedList<Vector3d> list2 = new LinkedList<Vector3d>();
//                    active.getTreeEffectorCoordinates(list2);
//                    System.out.println("Effectors");
//                    for(Vector3d v : list2){
//                    	System.out.println(v);
//                    }
//                }
                
            }
        }
        
        if (Mouse.isInsideWindow() && Mouse.isButtonDown(0)) {
        	if(!inPickMode){
	        	double xWorld = Mouse.getX() * (10d / 960d) - 5d;
	        	double yWorld = Mouse.getY() * (6d / 540d) - 3d;
	        	
	        	target.x = xWorld;
	        	target.y = yWorld;
	        	
	        	inSolveMode = true;
        	}
        	
        }
        if (Mouse.isInsideWindow() && !Mouse.isButtonDown(0)) {
        	inSolveMode = false;
        }
        
        

        if (Display.isCloseRequested()) {
            closeRequested = true;
        }
    }

    public void snapshot() {
        System.out.println("Taking a snapshot ... snapshot.png");

        GL11.glReadBuffer(GL11.GL_FRONT);

        int width = Display.getDisplayMode().getWidth();
        int height= Display.getDisplayMode().getHeight();
        int bpp = 4; // Assuming a 32-bit display with a byte each for red, green, blue, and alpha.
        ByteBuffer buffer = BufferUtils.createByteBuffer(width * height * bpp);
        GL11.glReadPixels(0, 0, width, height, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buffer );

        File file = new File("snapshot.png"); // The file to save to.
        String format = "PNG"; // Example: "PNG" or "JPG"
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
   
        for(int x = 0; x < width; x++) {
            for(int y = 0; y < height; y++) {
                int i = (x + (width * y)) * bpp;
                int r = buffer.get(i) & 0xFF;
                int g = buffer.get(i + 1) & 0xFF;
                int b = buffer.get(i + 2) & 0xFF;
                image.setRGB(x, height - (y + 1), (0xFF << 24) | (r << 16) | (g << 8) | b);
            }
        }
           
        try {
            ImageIO.write(image, format, file);
        } catch (IOException e) { e.printStackTrace(); }
    }
    
    /** 
     * Calculate how many milliseconds have passed 
     * since last frame.
     * 
     * @return milliseconds passed since last frame 
     */
    public int getDelta() {
        long time = (Sys.getTime() * 1000) / Sys.getTimerResolution();
        int delta = (int) (time - lastFrameTime);
        lastFrameTime = time;
     
        return delta;
    }

    private void createWindow() {
        try {
        	Display.setDisplayMode(new DisplayMode(960, 540));
            Display.setVSyncEnabled(true);
            Display.setTitle(windowTitle);
            Display.create();
        } catch (LWJGLException e) {
            Sys.alert("Error", "Initialization failed!\n\n" + e.getMessage());
            System.exit(0);
        }
    }
    
    /**
     * Destroy and clean up resources
     */
    private void cleanup() {
        Display.destroy();
    }
    
    public static void main(String[] args) {
        new InverseKinematicsApplication().run();
    }
    
}