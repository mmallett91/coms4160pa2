package edu.columbia.cs4160.pa2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

import org.ejml.simple.SimpleMatrix;
import org.lwjgl.opengl.GL11;

import edu.columbia.cs4160.pa2.inverse.Vector3d;

public class Link{
	
	// x y z position of joint in parent's coordinate system
	private double[] position;
	// 0 x
	// 1 y
	// 2 z
	private double[] dimensions;
	private double x, y, z;
	// rotation angle of revolute joint, degrees
	private double rotationZ;
	private double rotationY;
	
	private Link parent;
	private ArrayList<Link> children;
	
	private boolean active;
	
	public static boolean displayMesh = false;
	
	private boolean preventRotations;
	
	public Link(double[] position, double[] dimensions, Link parent){
		
		this.position = position;
		this.dimensions = dimensions;
		x = dimensions[0];
		y = dimensions[1]/2.0;
		z = dimensions[2]/2.0;
		
		this.parent = parent;
		children = new ArrayList<Link>();
		
		rotationZ = 0;
		rotationY = 0;
		
		active = false;
		
		preventRotations = false;
		
	}
	
	public void draw(){
		
//		GL11.glPopMatrix();
		
		
		GL11.glTranslated(position[0], position[1], position[2]);
		GL11.glRotated(rotationZ, 0, 0, 1);
		GL11.glRotated(rotationY, 0, 1 ,0);
		
		drawMesh();
		
		drawSkeleton();
		
		GL11.glPushMatrix();
		
		for(Link child : children){
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			child.draw();
		}
		
		GL11.glPopMatrix();
		
//		GL11.glPushMatrix();
		
		
	}
	
	private void drawMesh(){
		
		if(active || displayMesh){
			
			GL11.glBegin(GL11.GL_QUADS);
		
				if(active){
					GL11.glColor3f(0, 1, 0);
				}
				else{
					GL11.glColor3f(1, 1, 1);
				}
			
				GL11.glVertex3d(0, y, z);
				GL11.glVertex3d(0, -y, z);
				GL11.glVertex3d(0, -y, -z);
				GL11.glVertex3d(0, y, -z);
				
				GL11.glVertex3d(x, y, z);
				GL11.glVertex3d(x, -y, z);
				GL11.glVertex3d(x, -y, -z);
				GL11.glVertex3d(x, y, -z);
				
				GL11.glVertex3d(0, y, z);
				GL11.glVertex3d(x, y, z);
				GL11.glVertex3d(x, -y, z);
				GL11.glVertex3d(0, -y, z);
				
				GL11.glVertex3d(0, y, -z);
				GL11.glVertex3d(x, y, -z);
				GL11.glVertex3d(x, -y, -z);
				GL11.glVertex3d(0, -y, -z);
				
			GL11.glEnd();
		}
		
		
		
	}
	
	private void drawSkeleton(){
		
		drawSkeletonPoint();
		
		if(children.size() == 0){
		
			GL11.glBegin(GL11.GL_LINES);
			
				GL11.glColor3f(0, 0, 1);
				
				GL11.glVertex3d(0,0,0);
				GL11.glVertex3d(x,0,0);
		
			GL11.glEnd();
			
			GL11.glTranslated(x, 0, 0);
			
			drawSkeletonPoint();
			
		}
		
		else{
			GL11.glBegin(GL11.GL_LINES);
			
				GL11.glColor3f(0, 0, 1);
			
				for(Link child : children){
					double[] pos = child.getPosition();
					
					GL11.glVertex3d(0,0,0);
					GL11.glVertex3d(pos[0],pos[1],pos[2]);
					
				}
			
			GL11.glEnd();
			
		}
		
		
		
		
	}
	
	private void drawSkeletonPoint(){
		
		GL11.glBegin(GL11.GL_QUADS);
			
			GL11.glColor3f(0, 0, 1);
		
			GL11.glVertex3d(-.025, .025, .025);
			GL11.glVertex3d(-.025, -.025, .025);
			GL11.glVertex3d(-.025, -.025, -.025);
			GL11.glVertex3d(-.025, .025, -.025);
			
			GL11.glVertex3d(.025, .025, .025);
			GL11.glVertex3d(.025, -.025, .025);
			GL11.glVertex3d(.025, -.025, -.025);
			GL11.glVertex3d(.025, .025, -.025);
			
			GL11.glVertex3d(-.025, .025, .025);
			GL11.glVertex3d(.025, .025, .025);
			GL11.glVertex3d(.025, -.025, .025);
			GL11.glVertex3d(-.025, -.025, .025);
			
			GL11.glVertex3d(-.025, .025, -.025);
			GL11.glVertex3d(.025, .025, -.025);
			GL11.glVertex3d(.025, -.025, -.025);
			GL11.glVertex3d(.025, -.025, -.025);
			
		GL11.glEnd();
		
	}
	
	public void addChild(Link child){
		children.add(child);
	}
	
	public List<Link> getChildren(){
		return children;
	}
	
	public double[] getPosition(){
		return position;
	}
	
	public void setRotationZ(double r){
		if(!preventRotations){
			rotationZ = r;
		}
	}
	
	public double getRotationZ(){
		return rotationZ;
	}
	
	public void updateRotationZ(double delta){
		if(!preventRotations){
			rotationZ += delta;
		}
	}
	
	public void setRotationY(double r){
		if(!preventRotations){
			rotationY = r;
		}
	}
	
	public double getRotationY(){
		return rotationY;
	}
	
	public void setActive(boolean active){
		this.active = active;
	}
	
	public Link getParent(){
		return parent;
	}
	
	public SimpleMatrix getTreeEffectorCoordinates(LinkedList<Vector3d> list){
		
		SimpleMatrix transform;
		
		if(parent != null){
			transform = parent.getTreeEffectorCoordinates(list);
		}
		else{
			transform = SimpleMatrix.identity(4);
		}
		
		SimpleMatrix myTransform = getMyTransformMatrix();
		
		transform = transform.mult(myTransform);
		
		list.add(getMyEffectorCoordinates(transform));
		
		return transform;
		
	}
	
	public SimpleMatrix getTreeJointCoordinates(LinkedList<Vector3d> list){
		
		SimpleMatrix transform;
		
		if(parent != null){
			transform = parent.getTreeJointCoordinates(list);
		}
		else{
			transform = SimpleMatrix.identity(4);
		}
		
		SimpleMatrix myTransform = getMyTransformMatrix();
		
		transform = transform.mult(myTransform);
		
		list.add(getMyJointCoordinates(transform));
		
		return transform;
		
	}
	
	/*
	 * do all the transforms of drawing without actually drawing
	 * opengl keeps the matrix state between calls
	 */
	public Vector3d getMyJointCoordinates(SimpleMatrix transform){
	
		SimpleMatrix point = new SimpleMatrix(4, 1);
		point.set(0,0, 0);
		point.set(1,0, 0);
		point.set(2,0, 0);
		point.set(3,0, 1);
		
		SimpleMatrix transformed = transform.mult(point);
		
		Vector3d ret = new Vector3d(
			transformed.get(0),
			transformed.get(1),
			transformed.get(2)
		);
		
		return ret;
	}
	
	public Vector3d getMyEffectorCoordinates(SimpleMatrix transform){
		
		SimpleMatrix point = new SimpleMatrix(4, 1);
		point.set(0,0, x);
		point.set(1,0, 0);
		point.set(2,0, 0);
		point.set(3,0, 1);
		
		SimpleMatrix transformed = transform.mult(point);
		
		Vector3d ret = new Vector3d(
			transformed.get(0),
			transformed.get(1),
			transformed.get(2)
		);
		
		return ret;
		
	}
	
	private SimpleMatrix getMyTransformMatrix(){
		
		SimpleMatrix translate = SimpleMatrix.identity(4);
		translate.set(0,3, position[0]);
		translate.set(1,3, position[1]);
		translate.set(2,3, position[2]);
		
		double radians = (rotationZ * Math.PI) / 180d;
		
		SimpleMatrix rotate = SimpleMatrix.identity(4);
		rotate.set(0,0, Math.cos(radians));
		rotate.set(0,1, -1 * Math.sin(radians));
		rotate.set(1,0, Math.sin(radians));
		rotate.set(1,1, Math.cos(radians));
		
		return translate.mult(rotate);
		
	}
	
	public void getTreeHierarchy(Deque<Link> stack){
		
		stack.push(this);
		
		if(parent != null){
			parent.getTreeHierarchy(stack);
		}
		
	}
	
	public void preventRotations(){
		preventRotations = true;
	}
	
	public String toString(){
		
		return Arrays.toString(position) + " " + Arrays.toString(dimensions) + " " + rotationZ;
		
	}

}

