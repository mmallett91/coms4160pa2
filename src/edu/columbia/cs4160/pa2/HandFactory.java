package edu.columbia.cs4160.pa2;


public class HandFactory implements LinkTreeFactory {

	@Override
	public Link create() {

		double[] palmPos = {0,0,0};
        double[] palmDim = {1,.2, 1};
        
        double[] knucklePos = {1, 0, -.375};
        double[] knuckleDim = {.4,.2,.2};
        
        double[] fingerPos = {.4, 0, 0};
        double[] fingerDim = {.4, .2, .2};
        
        double[] knuckle2Pos = {1, 0, -.125};
        double[] knuckle3Pos = {1, 0, .125};
        double[] knuckle4Pos = {1, 0, .375};
        
        double[] thumbKnucklePos = {.2,0,-.5};
        
        Link palm = new Link(palmPos, palmDim, null);
        
        Link knuckle = new Link(knucklePos, knuckleDim, palm);
        palm.addChild(knuckle);
        
        Link finger1 = new Link(fingerPos, fingerDim, knuckle);
        knuckle.addChild(finger1);
        
        Link finger2 = new Link(fingerPos, fingerDim, finger1);
        finger1.addChild(finger2);
        
        Link knuckle2 = new Link(knuckle2Pos, knuckleDim, palm);
        palm.addChild(knuckle2);
        
        Link finger21 = new Link(fingerPos, fingerDim, knuckle2);
        knuckle2.addChild(finger21);
        
        Link finger22 = new Link(fingerPos, fingerDim, finger21);
        finger21.addChild(finger22);
        
        Link knuckle3 = new Link(knuckle3Pos, knuckleDim, palm);
        palm.addChild(knuckle3);
        
        Link finger31 = new Link(fingerPos, fingerDim, knuckle3);
        knuckle3.addChild(finger31);
        
        Link finger32 = new Link(fingerPos, fingerDim, finger31);
        finger31.addChild(finger32);
        
        Link knuckle4 = new Link(knuckle4Pos, knuckleDim, palm);
        palm.addChild(knuckle4);
        
        Link finger41 = new Link(fingerPos, fingerDim, knuckle4);
        knuckle4.addChild(finger41);
        
        Link finger42 = new Link(fingerPos, fingerDim, finger41);
        finger41.addChild(finger42);
        
        Link thumbKnuckle = new Link(thumbKnucklePos, fingerDim, palm);
        palm.addChild(thumbKnuckle);
        
        Link thumbFinger = new Link(fingerPos, fingerDim, thumbKnuckle);
        thumbKnuckle.addChild(thumbFinger);
        
        return palm;
		
	}

	@Override
	public String getModelName() {
		return "Hand";
	}

}
