package edu.columbia.cs4160.pa2;


public interface LinkTreeFactory {
	
	/**
	 * Initializes a forward kinematic tree
	 * @return the root Link of the tree
	 */
	public Link create();
	
	public String getModelName();

}
