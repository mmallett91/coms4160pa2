package edu.columbia.cs4160.pa2;

import java.io.IOException;
import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class ControlsTexture {
	
	public enum MODE {
		FORWARD, INVERSE
	}
	
	private static Texture forward;
	private static Texture inverse;
	
	public static void init(){
	
		try {
			forward = TextureLoader.getTexture("png",  ResourceLoader.getResourceAsStream("res/forward_controls.png"));
			inverse = TextureLoader.getTexture("png",  ResourceLoader.getResourceAsStream("res/inverse_controls.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void draw(MODE mode){
		
		Color.white.bind();
		
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		
		if(mode == MODE.FORWARD){
			forward.bind();
		}
		else{
			inverse.bind();
		}
		
		GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_FILL);
		
		GL11.glBegin(GL11.GL_QUADS);
			
			GL11.glTexCoord2f(0, 1); GL11.glVertex3f(-4f, -3f, 0); //bottom left
			GL11.glTexCoord2f(1, 1); GL11.glVertex3f(5.5f, -3f, 0); //bottom right
			GL11.glTexCoord2f(1, 0); GL11.glVertex3f(5.5f, 3f, 0); //top right
			GL11.glTexCoord2f(0, 0); GL11.glVertex3f(-4f, 3f, 0); //top left
			
		GL11.glEnd();
		
		GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
		
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		
	}
	
}
