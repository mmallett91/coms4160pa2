package edu.columbia.cs4160.pa2;


public class OneLinkFactory implements LinkTreeFactory {

	@Override
	public Link create() {
		
		double[] pos1 = {0,0,0};
		double[] pos2 = {1,0,0};
		  
		double[] dim = {1, .2, .2};
			
		Link link1 = new Link(pos1, dim, null);
		
		return link1;
	}

	@Override
	public String getModelName() {
		return "Single Link";
	}
	
	

}
