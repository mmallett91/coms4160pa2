package edu.columbia.cs4160.pa2;


public class SimpleLinearFactory implements LinkTreeFactory {

	@Override
	public Link create() {
		
		double[] pos1 = {0,0,0};
		double[] pos2 = {.4,0,0};
		  
		double[] dim = {.4, .2, .2};
			
		Link link1 = new Link(pos1, dim, null);
		
		Link link2 = new Link(pos2, dim, link1);
		link1.addChild(link2);
		
		Link link3 = new Link(pos2, dim, link2);
		link2.addChild(link3);
		
		Link link4 = new Link(pos2, dim, link3);
		link3.addChild(link4);
		
		Link link5 = new Link(pos2, dim, link4);
		link4.addChild(link5);
		
		Link link6 = new Link(pos2, dim, link5);
		link5.addChild(link6);
		
		Link link7 = new Link(pos2, dim, link6);
		link6.addChild(link7);
		
		return link1;
	}

	@Override
	public String getModelName() {
		return "Linear";
	}
	
	

}
